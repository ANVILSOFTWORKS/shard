﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Shard.Migrations
{
    /// <inheritdoc />
    public partial class addeddatalanes : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DataSliver_DataSliver_DataSliverId",
                table: "DataSliver");

            migrationBuilder.DropIndex(
                name: "IX_DataSliver_DataSliverId",
                table: "DataSliver");

            migrationBuilder.RenameColumn(
                name: "DataSliverId",
                table: "DataSliver",
                newName: "LaneId");

            migrationBuilder.AddColumn<int>(
                name: "LanePosition",
                table: "DataSliver",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "DataLane",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(type: "TEXT", maxLength: 127, nullable: true),
                    LaneOrder = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataLane", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataLane");

            migrationBuilder.DropColumn(
                name: "LanePosition",
                table: "DataSliver");

            migrationBuilder.RenameColumn(
                name: "LaneId",
                table: "DataSliver",
                newName: "DataSliverId");

            migrationBuilder.CreateIndex(
                name: "IX_DataSliver_DataSliverId",
                table: "DataSliver",
                column: "DataSliverId");

            migrationBuilder.AddForeignKey(
                name: "FK_DataSliver_DataSliver_DataSliverId",
                table: "DataSliver",
                column: "DataSliverId",
                principalTable: "DataSliver",
                principalColumn: "Id");
        }
    }
}
