﻿using Microsoft.EntityFrameworkCore;
using Shard.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Shard.Models
{
    public class DataSliver
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(127)]
        public string Title { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        [ForeignKey("DataSliver")]
        public int? ParentId { get; set; } = null;

        [ForeignKey("DataLane")]
        public int? LaneId { get; set; } = null;

        public int LanePosition { get; set; } = 1;

        public List<DataSliver> GetChildren(ShardContext _context)
        { 
            return _context.Set<DataSliver>()
                .Where(w => w.ParentId == Id)
                .ToList();
        }
    }
}
