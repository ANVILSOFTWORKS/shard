﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Shard.Data;
using Shard.Models;

namespace Shard.Controllers
{
    public class DataSliversController : Controller
    {
        private readonly ShardContext _context;

        public DataSliversController(ShardContext context)
        {
            _context = context;
        }

        // GET: DataSlivers
        public async Task<IActionResult> Index()
        {
            return View(await _context.DataSliver.ToListAsync());
        }

        // GET: DataSlivers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataSliver = await _context.DataSliver
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dataSliver == null)
            {
                return NotFound();
            }

            return View(dataSliver);
        }

        // GET: DataSlivers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DataSlivers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,ParentId,LaneId")] DataSliver dataSliver)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dataSliver);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dataSliver);
        }

        // GET: DataSlivers/Edit/5
        public async Task<IActionResult> Edit(int? id, int? CurrentId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataSliver = await _context.DataSliver.FindAsync(id);
            if (dataSliver == null)
            {
                return NotFound();
            }
            ViewBag.CurrentId = CurrentId;

            return View(dataSliver);
        }

        // POST: DataSlivers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, int? CurrentId, [Bind("Id,Title,Description,ParentId,LaneId")] DataSliver dataSliver)
        {
            if (id != dataSliver.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var dataBaseSliver = await _context.DataSliver.FindAsync(id);
                    //_context.Update(dataSliver);
                    dataBaseSliver.Title = dataSliver.Title;
                    dataBaseSliver.Description = dataSliver.Description;
                    dataBaseSliver.ParentId = dataSliver.ParentId;
                    dataBaseSliver.LaneId = dataSliver.LaneId;

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DataSliverExists(dataSliver.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("GetShard", "Home", new { Id = CurrentId });
            }
            return View(dataSliver);
        }

        // GET: DataSlivers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var dataSliver = await _context.DataSliver.FindAsync(id);
            if (dataSliver != null)
            {
                _context.DataSliver.Remove(dataSliver);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DataSliverExists(int id)
        {
            return _context.DataSliver.Any(e => e.Id == id);
        }

        public IActionResult Titles()
        {
            ViewData["Title"] = "Titles";

            var retVal = new StringBuilder("<tt>");

            AddRef(_context.Set<DataSliver>().Find(1));

            retVal.AppendLine("</tt>");

            return View("Plain", new HtmlString(retVal.ToString()));

            void AddRef(DataSliver node)
            {
                retVal.AppendLine("<p>" + node.Title + "</p>");

                node.GetChildren(_context)
                    .ForEach(f => AddRef(f));
            }
        }

        public IActionResult Descriptions()
        {
            ViewData["Title"] = "Descriptions";

            var retVal = new StringBuilder();

            AddRef(_context.Set<DataSliver>().Find(1));

            return View("Plain", new HtmlString(retVal.ToString()));

            void AddRef(DataSliver node)
            {
                if (node.Description != null && node.Description.Contains("<"))
                {
                    retVal.AppendLine(node.Description);
                }
                else
                {
                    retVal.AppendLine("<p>"+node.Description+"</p>");
                }
                node.GetChildren(_context)
                    .ForEach(f => AddRef(f));
            }
        }

        public IActionResult Both()
        {
            ViewData["Title"] = "Both";

            var retVal = new StringBuilder();

            AddRef(_context.Set<DataSliver>().Find(1));

            return View("Plain", new HtmlString(retVal.ToString()));

            void AddRef(DataSliver node)
            {
                retVal.AppendLine("<h4>"+node.Title+"</h4>");
                if (node.Description!= null && node.Description.Contains("<"))
                {
                    retVal.AppendLine(node.Description);
                }
                else
                {
                    retVal.AppendLine("<p>" + node.Description + "</p>");
                }
                node.GetChildren(_context)
                    .ForEach(f => AddRef(f));
            }
        }
    }
}
