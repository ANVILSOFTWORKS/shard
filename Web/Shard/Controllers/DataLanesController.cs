﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Shard.Data;
using Shard.Models;

namespace Shard.Controllers
{
    public class DataLanesController : Controller
    {
        private readonly ShardContext _context;

        public DataLanesController(ShardContext context)
        {
            _context = context;
        }


        public IActionResult GetLanes()
        { 

            return View(new LaneViewModel() { 
                Lanes = _context.Set<DataLane>().ToList(),
                Slivers = _context.Set<DataSliver>()
                    .Where(w => w.LaneId != null)
                    .ToList(),
            });
        }
        
        // GET: DataLanes
        public async Task<IActionResult> Index()
        {
            return View(await _context.DataLane.ToListAsync());
        }

        // GET: DataLanes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataLane = await _context.DataLane
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dataLane == null)
            {
                return NotFound();
            }

            return View(dataLane);
        }

        // GET: DataLanes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DataLanes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,LaneOrder")] DataLane dataLane)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dataLane);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dataLane);
        }

        // GET: DataLanes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataLane = await _context.DataLane.FindAsync(id);
            if (dataLane == null)
            {
                return NotFound();
            }
            return View(dataLane);
        }

        // POST: DataLanes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,LaneOrder")] DataLane dataLane)
        {
            if (id != dataLane.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dataLane);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DataLaneExists(dataLane.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dataLane);
        }

        // GET: DataLanes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var dataLane = await _context.DataLane.FindAsync(id);
            if (dataLane != null)
            {
                _context.DataLane.Remove(dataLane);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DataLaneExists(int id)
        {
            return _context.DataLane.Any(e => e.Id == id);
        }
    }
}
