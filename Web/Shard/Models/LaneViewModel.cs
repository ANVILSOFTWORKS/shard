﻿using System.Collections.Generic;

namespace Shard.Models
{
    public class LaneViewModel
    {
        public List<DataLane> Lanes { get; set; }
        public List<DataSliver> Slivers { get; set; }
    }
}
