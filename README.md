# Shard

This project is a little ticketing or information organization system, wherein a shard may contain other shards. It is easy to get the data out of the system using some of the titles, descriptions, or both views. I'm using it to write a novel in the snowflake method. It's like OneNote for people who only want to have text, and who can't stand OneNote, from the horrible input lag to the UI that looks and feel like mud-pie.

## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/ANVILSOFTWORKS/shard.git
git branch -M main
git push -uf origin main
```

## Tech stack
C#, MVC backend, backing onto sqlite.

## Status

- Currently doesn't have any auth layer
- Currently lacks support for fields that you'd need for a JIRA competitor.
- There's no drag and drop, which is the main thing preventing this from feeling like Trello did back when it wasn't a shitshow.

## Operationalizing

Please do not. 

## License

This is released under the HighlanderV2 license.