﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shard.Models;

namespace Shard.Data
{
    public class ShardContext : DbContext
    {
        public ShardContext (DbContextOptions<ShardContext> options)
            : base(options)
        {
        }

        public DbSet<Shard.Models.DataSliver> DataSliver { get; set; } = default!;
        public DbSet<Shard.Models.DataLane> DataLane { get; set; } = default!;
    }
}
