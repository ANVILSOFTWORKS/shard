﻿using Shard.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Shard.Models
{
    public class DataLane
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(127)]
        public string Title { get; set; }

        public int LaneOrder { get; set; }

        public List<DataSliver> GetLaneMembers(ShardContext _context)
        {
            return _context.Set<DataSliver>()
                .Where(w => w.LaneId == Id)
                .ToList();
        }
    }
}
