﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Shard.Data;
using Shard.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.Controllers
{

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ShardContext _context;

        public HomeController(ILogger<HomeController> logger, ShardContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return RedirectToAction("GetShard");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult GetShard(int? Id = 1)
        {
            var node = _context.Set<DataSliver>().Find(Id);

            return View(new Tuple<DataSliver, ShardContext>(node, _context));
        }


        public IActionResult Zoom(int Id)
        {
            return RedirectToAction("GetShard", new { Id = Id });
        }
        public IActionResult Remove(int Id, int CurrentId = 1)
        {
            RemoveSliver(Id);
            Save();

            return RedirectToAction("GetShard", new { Id = CurrentId });

            void RemoveSliver(int Id)
            {
                _context.Set<DataSliver>().Remove(
                    _context.Set<DataSliver>().Find(Id)
                );

                _context.Set<DataSliver>()
                    .Where(w => w.ParentId == Id)
                    .ToList()
                    .ForEach(f => RemoveSliver(f.Id));
            }
        }

        public IActionResult Create(int Id, int CurrentId = 1)
        {
            _context.Set<DataSliver>().Add(new DataSliver()
            {
                ParentId = Id,
                Title = "New Item",
                Description = "",
            });
            Save();

            return RedirectToAction("GetShard", new { Id = CurrentId });
        }

        public IActionResult MoveLeft(int Id, int CurrentId = 1)
        {
            var c = _context.Set<DataSliver>().Find(Id);
            var sibs = _context.Set<DataSliver>().Find(c.ParentId).GetChildren(_context)
                .OrderBy(o => o.LanePosition + o.Id).ToList();
            var i = sibs.IndexOf(c);

            if (i > 0)
            {
                var left = sibs[i - 1].Id + sibs[i - 1].LanePosition;
                c.LanePosition = left - Id - 1;
            }
            _context.SaveChanges();
            return RedirectToAction("GetShard", new { Id = CurrentId });
        }

        public IActionResult MoveRight(int Id, int CurrentId = 1)
        {
            var c = _context.Set<DataSliver>().Find(Id);
            var sibs = _context.Set<DataSliver>().Find(c.ParentId).GetChildren(_context)
                 .OrderBy(o => o.LanePosition + o.Id).ToList();
            var i = sibs.IndexOf(c);

            if (i > 0)
            {
                var right = sibs[i - 1].Id + sibs[i - 1].LanePosition;
                c.LanePosition = right - Id + 1;
            }
            _context.SaveChanges();
            return RedirectToAction("GetShard", new { Id = CurrentId });
        }

        private void Save()
        {
            _context.SaveChanges();
        }
    }
}
